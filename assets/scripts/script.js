function generateUsername() {
	let lname = document.querySelector("#lastName").value;
	let fname = document.querySelector("#firstName").value;
	let uname = fname;
	if (lname) {
		uname = fname[0].toLowerCase() + lname.toLowerCase();
	}

	if (username != undefined) {
		document.querySelector("#username").value = uname;
	}
}

// form validation from Bootstrap
(function() {
	"use strict";
	window.addEventListener(
		"load",
		function() {
			// Fetch all the forms we want to apply custom Bootstrap validation styles to
			let forms = document.getElementsByClassName("needs-validation");
			// Loop over them and prevent submission
			let validation = Array.prototype.filter.call(forms, function(form) {
				form.addEventListener(
					"submit",
					function(event) {
						if (form.checkValidity() === false) {
							event.preventDefault();
							event.stopPropagation();
						}
						form.classList.add("was-validated");
					},
					false
				);
			});
		},
		false
	);
})();

// for sidenav function
function openNav() {
	document.querySelector("#sidebar-nav").style.width = "250px";
	document.querySelector("#closebtn").style.display = "block";
	document.querySelector("#openbtn").style.display = "none";
	document.querySelector("#accounts").style.display = "block";
	document.querySelector("#profile").style.display = "block";
	document.querySelector("#events").style.display = "block";
	document.querySelector("#feedbacks").style.display = "block";
	document.querySelector("#logout").style.display = "block";
	document.querySelector("#register").style.display = "block";
	document.querySelector("#surveys").style.display = "block";
}

function closeNav() {
	document.querySelector("#sidebar-nav").style.width = "60px";
	document.querySelector("#closebtn").style.display = "none";
	document.querySelector("#openbtn").style.display = "block";
	document.querySelector("#accounts").style.display = "none";
	document.querySelector("#profile").style.display = "none";
	document.querySelector("#events").style.display = "none";
	document.querySelector("#feedbacks").style.display = "none";
	document.querySelector("#logout").style.display = "none";
	document.querySelector("#register").style.display = "none";
	document.querySelector("#surveys").style.display = "none";
}
