<?php 
session_start();

require './connection.php';

$event_id = $_POST['event_id'];
$eventName = $_POST['eventName'];
$eventDescription = $_POST['eventDescription'];
$eventRating = $_POST['eventRating'];
$eventImage = $_POST['eventImage'];

// Images
$filename = $_FILES['eventImage']['name'];
$filesize = $_FILES['eventImage']['size'];
$file_tmpname = $_FILES['eventImage']['tmp_name'];

$_SESSION["update_event_success"] = true;

$file_type = strtolower(pathinfo($filename, PATHINFO_EXTENSION));

$isImg = false;

if($file_type == "jpg" || $file_type == "png" || $file_type == "jpeg" || $file_type == "gif" || $file_type == "svg") {

		$isImg = true;
	}

if($filesize > 0 && $isImg == true) {
		
		$final_filepath = "./../assets/images/" . $filename;
		
		move_uploaded_file($file_tmpname, $final_filepath);
	} else {
		echo "please upload an image";
	}

$update_total_events = "UPDATE events SET name = '$eventName', description = '$eventDescription', rating = '$eventRating', image = '$final_filepath' WHERE id = '$event_id'";

$update_events_result = mysqli_query($conn, $update_total_events);

header("Location: ../views/admin_dashboard.php");
 ?>