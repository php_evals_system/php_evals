<?php
	session_start();
	include './connection.php';

	$eventName = htmlspecialchars($_POST['eventName']);
	$eventDescription = htmlspecialchars($_POST['eventDescription']);
	$eventRating = htmlspecialchars($_POST['eventRating']);


	
	// Images
	$filename = $_FILES['eventImage']['name'];
	$filesize = $_FILES['eventImage']['size'];
	$file_tmpname = $_FILES['eventImage']['tmp_name'];

	$_SESSION["add_event_success"] = true;
	// $_SESSION["add_event_error"] = try;


	$file_type = strtolower(pathinfo($filename, PATHINFO_EXTENSION));

	$isImg = false;
	$hasDetails = false;

	if ($eventName != "" && $eventDescription != "" && $eventRating) {
		$hasDetails = true;
	} else {

	}

	if($file_type == "jpg" || $file_type == "png" || $file_type == "jpeg" || $file_type == "gif" || $file_type == "svg") {

		$isImg = true;
	}

	if($filesize > 0 && $isImg == true && $hasDetails == true) {
		
		$final_filepath = "./../assets/images/" . $filename;
		
		move_uploaded_file($file_tmpname, $final_filepath);
	} else {
		echo "please upload an image";
	}

	$new_event_query = "INSERT INTO events (name, description, rating, image) VALUES ('$eventName', '$eventDescription', '$eventRating', '$final_filepath')";

	$result = mysqli_query($conn, $new_event_query);

	header("Location: ../views/admin_dashboard.php");

	if($result) {
		echo "Event added successfully";
	} else {
		echo mysqli_error($conn);
	}



?>