<?php 

	session_start();
	require "./connection.php";
	
	// sanitize the form inputs take the input field values using Global Variable $_POST
	$user = htmlspecialchars($_POST['user']);
	$password = sha1(htmlspecialchars($_POST['password']));


	$flag = false;
	$errors = 0;

	// validate the form
	if (!$user) {
		
		$_SESSION['errors']['account'] = "Username or Email is required";
		$errors = 1;

	}else{
		$_SESSION['form']['username'] = $user;

	}

	if (!$_POST['password']){

		$_SESSION['errors']['password'] = "Password is required";
		$errors = 1;

	} else{
		$_SESSION['form']['password'] = $_POST['password'];

	}

	$sql_query = "SELECT * FROM accounts WHERE email = '$user' OR username = '$user'";
	$results = mysqli_query($conn, $sql_query);
	

	$account = mysqli_fetch_assoc($results);


	$userEmail = $account['email'];
	$username = $account['username'];
	$userPassword = htmlspecialchars($account['password']);

	var_dump($account);
	echo ($account["role_id"]);
	// =========================================================

	
	// check the input fields
	if ($_POST['password'] || $_POST['user']) {
			# code...
		// check if user exists
		if($userEmail === $user || $username === $user){

			// check for the password and email or username
			// if true store user credential to session
			if($userEmail == $user && $userPassword == $password || $username == $user && $userPassword == $password) {

				$_SESSION['user']['username'] = $user;
				$_SESSION['user']['firstName'] = $account['first_name'];
				$_SESSION['user']['lastName'] = $account['last_name'];
				$_SESSION['user']['email'] = $account['email'];
				$_SESSION['user']['role'] = $account['role_id'];

				$flag = true;
			}

			// if password wrong
			if ($_POST['password']) {
				if($password != $userPassword){
					$_SESSION['errors']['password'] = "Wrong Password";
					$errors += 1;
				}
			}
			// if password field in empty
			else {
				$_SESSION['errors']['password'] = "Password is required";
				
				$errors += 1;
			}
		}

		// if account doesn't exist in the database
		else if($userEmail != $user || $username != $user){
			$_SESSION['errors']['account'] = "This acount does not exist";
			$errors += 1;
		}
	}


	// login is success header to home
	if($flag && $errors === 0) {
		unset($_SESSION['form']);
		$_SESSION['success']['login'] = "Welcome ".$_SESSION['first'];
		return header("Location:../views/home.php");
	}

	//login failed back to form
	return header("Location:".$_SERVER['HTTP_REFERER']);
		
