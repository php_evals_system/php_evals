<?php 
	require_once '../partials/header.php';

	function getTitle() {
		return "Profile";
	}

	$user = $_SESSION['user']['email'];

	$sql_query = "SELECT * FROM accounts WHERE email = '$user'";
	$results = mysqli_query($conn, $sql_query);
	$account = mysqli_fetch_assoc($results);
	?>

<section class="container-fluid mt-5">
	<div class="row mx-auto">
		<?php if (isset($_SESSION['user']) && $_SESSION['user']['role'] == 2): ?>

			<div class="col-12 col-md-2">
				<div class="card">
					<img class="card-img-top" src="../assets/images/avatar.png" alt="...">
					<header class="card-title font-weight-bold text-center">
						<?= $account['first_name']." ".$account['last_name']?>
					</header>
					<header class="card-title text-secondary text-center">
						<?= $account['email']?>
					</header>

				</div>
			</div>
		<?php endif ?>

		<div class="col-12 col-md-7 mx-auto">
			<div class="card">
				<div class="card-header">
					<header>Profile Information</header>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<tbody>
								<tr>
									<th scope="row" class="text-muted">				
										Name
									</th>
									<td>
										<?= $account["first_name"]." ".$account["last_name"]?>
									</td>
									<td>
										<a class="btn-link" data-toggle="modal" href="#editName">Edit</a>
									</td>
								</tr>
								<tr>
									<th scope="row" class="text-muted">				
										Email
									</th>
									<td>
										<?= $account["email"]?>
									</td>
									<td>
										<a class="btn-link" data-toggle="modal" href="#editEmail">Edit</a>
									</td>

								</tr>
								<tr>
									<th scope="row" class="text-muted">				
										Username
									</th>
									<td>
										<?= $account["username"]?>
									</td>
									<td>
										<a class="btn-link" data-toggle="modal" href="#editUsername">
											Edit
										</a>
									</td>

								</tr>
								<tr>
									<th scope="row" class="text-muted">				
										Password
									</th>
									<td>
										<a href="#changePassword" class="btn-link" data-toggle="modal">Change Password</a>
									</td>
									<td></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

		<?php if (isset($_SESSION['user']) && $_SESSION['user']['role'] == 2): ?>


			<div class="col-12 col-md-3">
				<div class="card">
					<div class="list-group">
						<a href="./events.php" class="list-group-item list-group-item-action">
							Events
						</a>
						<a href="./survey.php" class="list-group-item list-group-item-action">Feedbacks</a>
						
					</div>
					
				</div>

			</div>
		<?php endif ?>
	</div>
</section>



<!--Edit Name Modal -->
<div class="modal fade" id="editName" tabindex="-1" role="dialog" aria-labelledby="editNameTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
	    <div class="modal-content">
	    	<form
	    		method="POST"
	    		<?php /*send the id of account and what to edit*/ ?>
	    		action="../controllers/update.php?id=<?=$account['id'];?>&edit=name">
				<div class="modal-header">
			        <h5 class="modal-title" id="editNameTitle">Name</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			    </div>
		    	<div class="modal-body">
		    		<div class="container">
		    			
			    		<div class="form-group">
			    			<label for="fname">First Name</label>
			        		<input type="text" id="fname" name="firstName" class="form-control" value="<?= $account['first_name']?>">
			    		</div>
			    		<div class="form-group">
			    			<label for="lname">Last Name</label>
			        		<input type="text" id="lname" name="lastName" class="form-control" value="<?= $account['last_name']?>">
			    		</div>
		    		</div>
		    	</div>
				<div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">
			        	Cancel
			        </button>
			        <button type="submit" class="btn btn-primary">Save changes</button>
				</div>
	    	</form>
	    </div>
	</div>
</div>

<!-- Edit Email -->
<div class="modal fade" id="editEmail" tabindex="-1" role="dialog" aria-labelledby="editEmailTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
	    <div class="modal-content">
	    	<form method="POST" action="../controllers/update.php?id=<?=$account['id'];?>">
				<div class="modal-header">
			        <h5 class="modal-title" id="editEmailTitle">Email</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			    </div>
		    	<div class="modal-body">
		    		<div class="container">
		    			
			    		<div class="form-group">
			    			<label for="email">Email</label>
			        		<input type="email" id="email" name="email" class="form-control" value="<?= $account['email']?>">
			    		</div>
			    	</div>
			    		
		    	</div>
				<div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">
			        	Cancel
			        </button>
			        <button type="submit" class="btn btn-primary">Save changes</button>
				</div>
	    	</form>
	    </div>
	</div>
</div>

<!-- Edit Username -->
<div class="modal fade" id="editUsername" tabindex="-1" role="dialog" aria-labelledby="editUsernameTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
	    <div class="modal-content">
	    	<form method="POST" action="../controllers/update.php?id=<?=$account['id'];?>">
				<div class="modal-header">
			        <h5 class="modal-title" id="editUsernameTitle">Modal title</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			    </div>
		    	<div class="modal-body">
		    		<div class="container">
		    			
			    		<div class="form-group">
			    			<label for="username">Username</label>
			        		<input type="text" id="username" name="username" class="form-control" value="<?= $account['username']?>">
			    		</div>
			    	</div>
			    		
		    	</div>
				<div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">
			        	Cancel
			        </button>
			        <button type="submit" class="btn btn-primary">Save changes</button>
				</div>
	    	</form>
	    </div>
	</div>
</div>

<!-- Change Password -->
<div class="modal fade" id="changePassword" tabindex="-1" role="dialog" aria-labelledby="changePasswordTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
	    <div class="modal-content">
	    	<form method="POST" action="../controllers/update.php?id=<?=$account['id'];?>">
				<div class="modal-header">
			        <h5 class="modal-title" id="changePasswordTitle">Modal title</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			    </div>
		    	<div class="modal-body">
		    		<div class="container">	
			    		<div class="form-group">
			    			<label for="oldPassword">Old Password</label>
			        		<input type="text" id="oldPassword" name="oldPassword" class="form-control">
			    		</div>
	    				<div class="form-group">
	    					<label for="newPassword">Username</label>
	    		    		<input type="text" id="newPassword" name="newPassword" class="form-control">
	    				</div>
			    	</div>
			    		
		    	</div>
				<div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">
			        	Cancel
			        </button>
			        <button type="submit" class="btn btn-primary">Save changes</button>
				</div>
	    	</form>
	    </div>
	</div>
</div>

<?php require_once '../partials/footer.php'; ?>