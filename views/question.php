<?php 
require_once("../partials/header.php");



function getTitle() {
	return "Questions";
}
?>

<section class="container mt-5">
	
	<div class="card p-3">
		<div class="container-fluid">
			<div class="row">
				<div class="col-3">
					<div class="card ">
						<div class="card-header">
							Create Survey Question
						</div>
						<div class="card-body">	
							<form action="../controllers/add_questions.php" method="POST" enctype="multipart/form-data">
								<div class="input-group">
									<label for="questionName">Question</label>
								</div>
								<div class="input-group">
									<input type="text" id="questionName" name="question" placeholder="Enter your Question" class="form-control">
								</div>
								<div class="input-group">
									<button type="submit" class="btn btn-outline-success btn-block mt-3">Add Question</button>
								</div>
							</form>
						</div>
					</div>
				</div>



				<div class="col-9">
					<table class="table table-striped table-border">
						<thead class="thead-dark">
							<tr>
								<th scope="col">Questions</th>
								<th scope="col" colspan="2" class="text-center">Action</th>
							</tr>
						</thead>


						<?php 
						$question_query = "SELECT * FROM questions";

				// var_dump($product_query);

				// mysqli_query() performs a query to our db that returns true of false
				// mysqli_query(connection, query to execute)
						$question_array = mysqli_query($conn, $question_query);
				// var_dump($product_array);


						foreach($question_array as $key => $question){
					// var_dump($prod);

							?>


							<tbody>
								<tr>
									<td><?= $question['question']; ?></td>
									<td colspan="2" class="">
										<p>
											<button type="button" class="btn btn-warning btn-block"	data-toggle="modal" data-target="#id<?php echo $key+1?>">Update Question</button>

											<a href="../controllers/delete_question.php? id=<?=$question['id'];?>" class="btn btn-danger btn-block mt-2">Delete Question</a>
										</p>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<?php foreach ($question_array as $key => $question): ?>	
		<!-- Modal -->
		<div class="modal fade" id="id<?php echo $key+1?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title text-center" id="exampleModalLongTitle">Edit Question</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<form action="../controllers/update_question.php?id=<?=$question['id'];?>" method="POST" enctype="multipart/form-data">
							<div class="input-group">
								<label for="questionName">Question</label>
							</div>
							<div class="input-group">
								<input type="text" id="questionName" name="question" placeholder="Enter your Question" class="form-control" value="<?= $question['question']?>">
							</div>
							<button type="submit" class="btn btn-success btn-block mt-3">Update</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

<?php endforeach ?>

</section>

<?php require_once("../partials/footer.php") ?>