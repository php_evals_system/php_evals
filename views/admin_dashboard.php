<?php  
	
	require_once '../partials/header.php';

	function getTitle() {
		return "Admin Dashboard";
	}

?>

	<?php

	$select_query = "SELECT * FROM events";

	$result = mysqli_query($conn, $select_query);

	?>
	<section class="container-fluid">
		<div class="row">
			<div class="col-md-8 mx-auto">
				<?php
				// This sessions will be the alert if user successfully added and updated an event. The session will start from their respective controllers.
				if(isset($_SESSION["add_event_success"])) {
					if($_SESSION["add_event_success"]) {
						echo "<div class='alert alert-success'>
						<strong>Success, </strong> event added.
						</div>
						";
					}
					unset($_SESSION["add_event_success"]);
				}

				if(isset($_SESSION["update_event_success"])) {
					if($_SESSION["update_event_success"]) {
						echo "<div class='alert alert-success'>
						<strong>Success, </strong> event updated.
						</div>
						";
					}
					unset($_SESSION["update_event_success"]);
				}

				
				?>
				

				<?php if (isset($_SESSION['user']) && $_SESSION['user']['role'] == 1): ?>
					
					<div class="card create-event-form">

						<div class="card-header">
							<h2 class="text-center card-title"> Create an Event </h2>
						</div>

						<form action="../controllers/add_event.php" method="POST" enctype="multipart/form-data">
							
							<div class="card-body">
								<div class="form-group position-relative">
									<input type="text" id="eventName" name="eventName" class="form-control" placeholder="Event Name">
									<label for="eventName">Event Name </label>
								</div>

								<div class="form-group position-relative">
									<input placeholder="Event Description" id="eventDescription" name="eventDescription" class="form-control">
									<label for="eventDescription"> Event Description </label>
								</div>

								<div class="form-group position-relative">
									<input placeholder="Event Rating" id="eventRating" name="eventRating" class="form-control">
									<label for="eventRating"> Event Rating </label>
								</div>

								<div class="form-group position-relative">
									<input type="file" placeholder="text" id="eventImage" name="eventImage" class="form-control">
									<label for="eventImage"> Event Image </label>
								</div>
							</div>

							<div class="card-footer">
								<button type="submit" class="btn btn-primary btn-block">Create Event</button>
							</div>
						</form>
					</div>
				</div> <!-- end column -->
				<!-- START OF EVENT QUESTIONS -->
			<?php endif ?>

			<!-- <div class="col-md-2 mr-2">
				<?php 
				// 	if(isset($_SESSION["add_question_success"])) {
				// 		if($_SESSION["add_question_success"]) {
				// 			echo "<div class='alert alert-success'>
				// 			<strong>Success, </strong> question added.
				// 			</div>
				// 			";
				// 		}
				// 		unset($_SESSION["add_question_success"]);
				// }
				?>
				<div class="card">
					<div class="card-header">
						<h2 class="text-center card-title"> Create an Event Question </h2>
					</div>

					<form action="../controllers/add_question.php" method="POST">

						<div class="card-body">
							<div class="form-group position-relative">
								<input type="text" id="eventQuestion" name="eventQuestion" class="form-control" placeholder="Event Question">
								<label for="eventQuestion">Event Question </label>
							</div>
						</div>

						<div class="card-footer">
							<button type="submit" class="btn btn-primary btn-block">Add Question</button>
						</div>

					</form>
				</div>
			</div> -->
			<!-- END OF EVENT QUESTIONS -->

		</div> <!-- end row -->
	</section> <!-- end container -->
	<hr>
	<section class="container-fluid">
		<div class="row">
			<div class="col-md-7 mt-5 mx-auto">
				<div class="card">
					 <div class="card-header">
					 	<h2 class="text-center card-title"> List of events </h2>
					 </div>
				</div>
				 <table class="table">
				 	<thead>
				 		<tr>
				 			<th scope="col">Event Name</th>
				 			<th scope="col">Event Description</th>
				 			<th scope="col">Event Rating</th>
				 			<th scope="col">Event Image</th>
				 			<?php if (isset($_SESSION['user']) && $_SESSION['user']['role'] == 1): ?>
				 				<th scope="col">Action</th>
							<?php endif ?>
				 		</tr>
				 	</thead>
				 	<tbody>
				 		<?php
				 		foreach($result as $newEvent) {
		 				?>
		 				<tr>
							<th scope="row"><?php echo $newEvent['name']?></th>
							<td scope="col"><?php echo $newEvent['description']?></td>
							<td scope="col"><?php echo $newEvent['rating']?></td>
							<td scope="col"><img id="evntImg" src="<?php echo $newEvent['image']; ?>" ></td>
							<?php if (isset($_SESSION['user']) && $_SESSION['user']['role'] == 1): ?>
								<td>
									<a href='../controllers/delete_event.php?id=<?php echo $newEvent['id']; ?>' class="btn btn-danger btn-block">Delete</a>
									<a href='./edit_event.php?id=<?php echo $newEvent['id']; ?>' class="btn btn-info btn-block">Update</a>
								</td>
							<?php endif ?>
						</tr>
						<?php
				 			}
				 		?>
				 		
				 	</tbody>
				 </table>
			</div>
		</div>
	</section>

<?php require_once '../partials/footer.php'; ?>