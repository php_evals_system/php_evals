<?php 
	require_once '../partials/header.php';

	function getTitle() {
		return "Home";
	}

	$accounts_query = "SELECT * FROM accounts";
	$accounts =  mysqli_query($conn, $accounts_query);

 ?>


<section class="container mt-5">

	<div class="row mx-auto">
		<div class="col-md-4">
			<div class="card">
				<div class="card-header">
					<header>Add Account</header>
				</div>
					<form method="post" action="../controllers/register.php" novalidate class="needs-validation">
						<div class="card-body">
								
							<div class="form-group form-group-label position-relative">
								<input
									type="text"
									class="form-control"
									id="firstName"
									name="firstName"
									placeholder="First Name"
									onblur="generateUsername()"
									required
									>
								<label for="firstName">First Name</label>
								<div class="valid-feedback">
							        Looks good!
							    </div>
							    <div class="invalid-feedback">
							        First Name is required
							    </div>
							</div>
							<!--  -->
							<div class="form-group form-group-label position-relative">
								<input
									type="text"
									class="form-control"
									id="lastName"
									name="lastName"
									placeholder="Last Name" 
									onblur="generateUsername()"
									required

									>
								<label for="lastName">Last Name</label>
								<div class="valid-feedback">
							        Looks good!
							    </div>
							    <div class="invalid-feedback">
							        Last Name is required
							    </div>
							</div>
							<div class="form-group form-group-label position-relative">
								<input
									type="text"
									class="form-control"
									id="email"
									name="email"
									placeholder="Email"
									required

									>
								<label for="email">Email</label>
								<div class="valid-feedback">
							        Looks good!
							    </div>
							    <div class="invalid-feedback">
							        Email is required
							    </div>
							</div>

							<div class="form-group form-group-label position-relative">
								<input
									type="text"
									class="form-control"
									id="username"
									name="username"
									placeholder="username"
									required>
								<label for="username">Username</label>
								<div class="valid-feedback">
							        Looks good!
							    </div>
							    <div class="invalid-feedback">
							        Please choose a username
							    </div>
							</div>
							<!--  -->
							<div class="form-group">
								<label  class="text-secondary" for="role">Role:</label>
								<select class="custom-select" name="role" id="role">
									<option value="2" selected>Student</option>
									<option value="1">Admin</option>
								</select>
							</div>
							<!--  -->
					</div>

					<div class="card-footer">
						<button type="submit" class="btn btn-primary btn-block">Add</button>
					</div>
				</form>
				
			</div>
		</div>
		<div class="col-md-8 ">
			<div class="card">
				<div class="card-header">
					<header class="font-weight-bold">Accounts</header>
				</div>
				
				<div class="card-body">
					
					<div class="table-responsive">
						<table class="table  table-hover">
							<thead>
								<tr>
									<th scope="col">#</th>
									<th scope="col">First Name</th>
									<th scope="col">Last Name</th>
									<th scope="col">Email</th>
									<th scope="col">Role</th>
									<th scope="col">Action</th>
								</tr>
							</thead>
							<tbody>
				
								<?php foreach ($accounts as $key => $account): ?>
									
								<tr>
									<th scope="row"><?= $key + 1 ?></th>
									<td><?= $account['first_name'] ?></td>
									<td><?= $account['last_name'] ?></td>
									<td><?= $account['email'] ?></td>
									<td><?= $account['role_id'] == 1 ? "Admin" : "Student"?></td>
									<td>
										<a href="../controllers/delete_account.php?id=<?=$account['id']?>" class="btn-link">Delete</a>
									</td>
								</tr>
								<?php endforeach ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			
		</div>
		<div class="col-md-2"></div>
	</div>

</section>

<?php require_once '../partials/footer.php'; ?>

