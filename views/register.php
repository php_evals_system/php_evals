<?php  
	
	require_once '../partials/header.php';

	function getTitle() {
		return "Register";
	}

?>

	<section class="container">

		<div class="row">
			<div class="col-md-7 mx-auto mt-5">

				<div class="card card-form shadow">

					<div class="card-header">
						<h2 class="text-center card-title" id="test"> Register </h2>
					</div>
					
					<form action="../controllers/register.php" method="POST" class="needs-validation" novalidate>

						<div class="card-body">
							<div class="form-row">
								<div class="col-lg-6">
								
									<div class="form-group form-group-label position-relative">
										<input 
											placeholder="First Name"
											type="text" id="firstName" 
											name="firstName" class="form-control" 
											onblur="generateUsername()" 
											required>
										<label for="firstName">First Name </label>
										<div class="valid-feedback">
									        Looks good!
									    </div>
									    <div class="invalid-feedback">
									        First Name is needed
									    </div>
									</div>

								</div>

								<div class="col-lg-6">
									<div class="form-group form-group-label position-relative">
										<input
											placeholder="Last Name" 
											type="text" id="lastName" 
											name="lastName" 
											class="form-control"
											onblur="generateUsername()"
											required>
										<label for="lastName">Last Name </label>
										<div class="valid-feedback">
									        Looks good!
									    </div>
									    <div class="invalid-feedback">
									        Last Name is needed
									    </div>
									</div>
								</div>

							</div>


							<div class="form-group form-group-label position-relative">
								<input
									type="email" 
									id="email" 
									name="email" 
									class="form-control" 
									placeholder="Email"
									required>
								<label for="email">Email </label>
								<div class="valid-feedback">
									Looks good!
							    </div>
							    <div class="invalid-feedback">
							        Email is needed
							    </div>
							</div>

							<div class="form-group form-group-label position-relative">
								<input
									type="text"
									class="form-control"
									id="username"
									name="username"
									placeholder="username"
									required>
								<label for="username">Username</label>
								<div class="valid-feedback">
							        Looks good!
							    </div>
							    <div class="invalid-feedback">
							        Please choose a username
							    </div>
							</div>
							<div class="form-group">
								<label  class="text-secondary" for="role">Role:</label>
								<select class="custom-select" name="role" id="role">
								  <option value="1">Admin</option>
								  <option value="2" selected>Student</option>
								</select>
							</div>



							<a href="#">Back to Dashboard</a>
						</div>

						<div class="card-footer">
							<button type="submit" class="btn btn-primary btn-block"> Register </button>
						</div>

					</form> <!-- end form -->

				</div>

			</div> <!-- end cols -->
		</div> <!-- end row -->
	</section> <!-- end container -->

<?php require_once '../partials/footer.php'; ?>
