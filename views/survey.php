<?php 

require_once("../partials/header.php");

function getTitle() {
		return "Admin Survey";
	}

 ?>



<div class="container">
			<div class="row">
				<div class="col-12">
					<h1>Survey</h1>
					<div class="form">
					<table class="table table-striped table-bordered ">
						<thead class="thead-dark">
						    <tr>
						      
						      <th scope="row" id="questions">Questions</th>
						      <th scope="row" class="text-center">Low</th>
						      <th scope="row" class="text-center">Semi-Low</th>
						      <th scope="row" class="text-center">Good</th>
						      <th scope="row" class="text-center">Very Good</th>
						      <th scope="row" class="text-center">Satisfaction</th>
						    </tr>
						  </thead>

						<?php 
							$question_query = "SELECT * FROM questions";

							// var_dump($product_query);

							// mysqli_query() performs a query to our db that returns true of false
							// mysqli_query(connection, query to execute)
							$question_array = mysqli_query($conn, $question_query);
							// var_dump($product_array);


							foreach($question_array as $key => $question){
								// var_dump($prod);

						?>
						  <tbody>
						  	<tr>
						  		<form class="form-checkbox">
						  			<td><?= $question['question']; ?></td>
							  		<td>	
							  			  <input class="form-check-input inlineCheckbox1" type="checkbox" id="inlineCheckbox1" name="checkBox" value="1" onclick="selectOnlyThis(this)">
							  		</td>
							  		<td>
							  			  <input class="form-check-input inlineCheckbox2" type="checkbox" id="inlineCheckbox2" name="checkBox" value="2" onclick="selectOnlyThis(this)">
							  		</td>
							  		<td>
							  			  <input class="form-check-input inlineCheckbox3" type="checkbox" id="inlineCheckbox3" name="checkBox" value="3" onclick="selectOnlyThis(this)">
							  		</td>
							  		<td>
							  			  <input class="form-check-input inlineCheckbox4" type="checkbox" id="inlineCheckbox4" name="checkBox" value="4" onclick="selectOnlyThis(this)">
							  		</td>
							  		<td>
							  			  <input class="form-check-input inlineCheckbox5" type="checkbox" id="inlineCheckbox5" name="checkBox" value="5" onclick="selectOnlyThis(this)">
							  		</td>
						  		</form>
						  	<?php } ?>
						  	</tr>
						  </tbody>
					</table>
					</div>		
				</div>
			</div>
		</div>



<?php require_once("../partials/footer.php") ?>